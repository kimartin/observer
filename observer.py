# https://gitlab.com/kimartin/observer
# check this address for updates to the script

import os
import datetime
import logging
import sys
from pynput.keyboard import Key, Listener

numpad = {f'<{96 + i}>': f'{i}' for i in range(10)}

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                    level=logging.DEBUG,
                    stream=sys.stdout)

from time import time

def get_active_window():
    """
    Get the currently active window on any platform
    https://stackoverflow.com/a/36419702

    Returns
    -------
    string :
        Name of the currently active window.
    """
    active_window_name = None
    if sys.platform in ['linux', 'linux2']:
        # NB: untested on Linux
        try:
            import wnck
        except ImportError:
            logging.info("wnck not installed")
            wnck = None
        if wnck is not None:
            screen = wnck.screen_get_default()
            screen.force_update()
            window = screen.get_active_window()
            if window is not None:
                pid = window.get_pid()
                with open("/proc/{pid}/cmdline".format(pid=pid)) as f:
                    active_window_name = f.read()
        else:
            try:
                from gi.repository import Gtk, Wnck
                gi = "Installed"
            except ImportError:
                logging.info("gi.repository not installed")
                gi = None
            if gi is not None:
                Gtk.init([])  # necessary if not using a Gtk.main() loop
                screen = Wnck.Screen.get_default()
                screen.force_update()  # recommended per Wnck documentation
                active_window = screen.get_active_window()
                pid = active_window.get_pid()
                with open("/proc/{pid}/cmdline".format(pid=pid)) as f:
                    active_window_name = f.read()
    elif sys.platform in ['Windows', 'win32', 'cygwin']:
        try:
            from ctypes import wintypes, windll, create_unicode_buffer
        except ImportError:
            logging.info("Missing ctypes import.")
            ctypes = None
        if ctypes is not None:
            hWnd = windll.user32.GetForegroundWindow()
            length = windll.user32.GetWindowTextLengthW(hWnd)
            buf = create_unicode_buffer(length + 1)
            windll.user32.GetWindowTextW(hWnd, buf)
            active_window_name = buf.value if buf.value else None

    elif sys.platform in ['Mac', 'darwin', 'os2', 'os2emx']:
        # http://stackoverflow.com/a/373310/562769
        try:
            from AppKit import NSWorkspace
            from Quartz import CGWindowListCopyWindowInfo, kCGWindowListOptionOnScreenOnly, kCGNullWindowID
        except ImportError:
            logging.info("Some imports missing: AppKit or Quartz")
        curr_pid = NSWorkspace.sharedWorkspace().activeApplication()['NSApplicationProcessIdentifier']
        windowList = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly, kCGNullWindowID)
        windowList = {win['kCGWindowOwnerPID']: win["kCGWindowOwnerName"] for win in windowList}
        active_window_name = windowList.get(curr_pid, u'Unknown')
        # for window in windowList:
        #     if curr_pid == window['kCGWindowOwnerPID']:
        #         active_window_name = window.get('kCGWindowOwnerName', u'Unknown')
        #         break
    else:
        print("sys.platform={platform} is unknown. Please report."
              .format(platform=sys.platform))
        print(sys.version)
    return active_window_name


class Observer:
    def __init__(self,
                 start_key,
                 next_key,
                 defaults,
                 stop_key=None,
                 default_category="comment",
                 states=None,
                 sep=",",
                 suppress=True,
                 filename='Events',
                 **kwargs):
        """
        Creates an Observer instance that allows logging of events keyed using dictionaries.
        Importantly, the Observer instance is case-sensitive and will not detect uppercase characters as their
            corresponding lowercase characters (i.e. pressing A by having Capslock enabled will attempt to find an event
            keyed to A, and default to the "defaults" argument if not found, even if an event is keyed to a).

        :param start_key: key to press to signal the start and end of logging
        :param next_key: key signaling the end of an observation (i.e. the nextline character)
        :param stop_key: key to stops the Observer instance when released. Defaults to None, where it will the key
            defined in start_key will be used.
        :param defaults: for each category the default to use when pressing a key that is not in the event-pair
            dictionary (e.g. if pressing X when no event is keyed to X). Should be either a list in the same order as
            the event-key dictionaries, or a dictionary with keys corresponding to the names of the kwargs/events dictionaries
        :param events: dictionary of arbitrary depth that determines the events to be logged.
            for each depth, the name of the dictionary (specifically, the first character of it) is used to create a
            selector to tell the Observer to look into that dictionary
        :param default_category: category to default to
        :param states: events to be treated as states, initiated at False and toggled on/off (resp True/False)
        :param sep: character to use to separate the categories
        :param suppress: bool. Passed to the Listener instance. If True (the default), prevents key presses from being
        transmitted out of the program (i.e. you cannot type in another window).
        :param kwargs: Dictionaries of the key-events pairs. These dictionaries must be keyworded, as the Observer
            instance uses the first character of the keywords to generate the selector (e.g. if passed a dictionary dict,
            use the d key to select it, then further keys to select values in dict)
        """
        # Run-wide parameters (fixed at start time)
        self.now = str(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
        self.path = os.path.basename(__file__)
        self.out_file = f"{self.now}{'_' if filename not in ['', None] else ''}{filename}.txt"

        self.suppress = suppress
        self.start_key = start_key
        self.next_key = next_key
        self.stop_key = stop_key or start_key
        self.sep = sep
        self.ignore_keys = {
            Key.esc,
            Key.enter,
            Key.caps_lock,
            Key.tab,
            Key.down, Key.up, Key.left, Key.right,
            Key.delete, Key.end, Key.home,
            Key.media_next, Key.media_previous, Key.media_play_pause,
            Key.media_volume_up, Key.media_volume_down, Key.media_volume_mute,
            Key.ctrl, Key.ctrl_l, Key.ctrl_r,
            Key.shift, Key.shift_r, Key.shift_l,
            Key.alt, Key.alt_r, Key.alt_gr, Key.alt_l,
            Key.cmd, Key.cmd_r, Key.cmd_l
        }

        self.started = False

        # Loop parameters (change once per press of next_key)
        self.notes = list()
        self.time = None

        # Parameters of the category dictionaries
        #   (i.e. self.names will be of the form {"b": bird,...}
        self.names = {k[0]: k.capitalize() for k in kwargs}
        self.defaults = {k.capitalize(): v.capitalize() for k, v in defaults.items() if k in kwargs}
        self.default_category = default_category.capitalize()
        self.events = {k.capitalize(): v for k, v in kwargs.items()}
        self.states = {st: False for st in states} or dict()

    def writerow(self):
        if len(self.notes) == 0:
            return None

        # if the secod element of the row is in states, reverse it
        if self.notes[1].startswith(tuple(self.states.keys())):
            self.states[self.notes[1]] = not self.states[self.notes[1]]
            self.notes[-1] += f' {str(self.states[self.notes[1]])}'

        # add to the events log and reset
        with open(self.out_file, 'a', newline='') as logger:
            logger.write(f"{self.sep}".join([str(self.time)] + self.notes) + '\n')

        # reset
        self.time = None
        self.notes = list()
        self.category = None

    def repeat_latest_matching_row(self):
        # runs through the log from the most recent entry and take the first partial match
        # if the notes are empty, takes the last entry
        with open(self.out_file, 'r') as logged:
            for row in reversed(logged.readlines()):
                row = row.replace("\n", "").split(self.sep)[1:]
                if "".join(self.notes) in "".join(row):
                    self.notes = row
                    # replace notes with first partial match
                    # check if the second element of self.notes in is self.states
                    if row[1] in self.states:
                        self.states[row[1]] = not bool(row[2])
                        row[2] = str(not bool(row[2]))
                    break

    def remove_last_key(self):
        if len(self.notes) > 1:
            self.notes.pop()
        else:
            self.time = None
            self.notes = list()

    def add_to_notes(self, char):
        # first select the category
        if len(self.notes) == 0:
            if char == self.start_key:
                char = "Stop" if self.started else "Start"
                self.started = True
            else:
                char = self.names.get(char, self.default_category)

        # then select the specific instance within the category
        elif len(self.notes) == 1:
            if self.notes[0] in self.events:
                char = self.events[self.notes[0]].get(char, self.defaults[self.notes[0]])
            else:
                # this is only used when self.notes[0] == self.default_category
                self.notes.append('')

        if len(self.notes) > 2:
            self.notes[-1] += char
        else:
            self.notes.append(char)

    def add_space(self):
        if len(self.notes) > 2:
            self.notes[-1] += ' '

    def ignore(self):
        pass

    def on_press(self, key):
        # bit wonky on Mac, get_active_window() actually returns PyCharm
        win = get_active_window()
        if self.path in win or 'PyCharm' in win:
            if key == self.stop_key:
                return False

            # when any key is pressed after a reset, take the current system time
            if self.time is None and key not in self.ignore_keys:
                self.time = datetime.datetime.now()

            if key == self.next_key:
                self.writerow()
                # print state events that are currently True at the end of the line
                print(self.sep, [key for key, value in self.states.items() if value])

            elif key == Key.backspace:
                self.remove_last_key()
            elif key == Key.space:
                self.add_space()
            elif key in self.ignore_keys:
                self.ignore()

            else:
                char = numpad.get(str(key), key.char)
                if char == "":
                    self.repeat_latest_matching_row()
                elif char:
                    self.add_to_notes(char)

            # Every time a key is pressed, print the formatted notes in place, replacing previous displays
            print('\r', *self.notes, sep=self.sep, end="", flush=True)

        return None


    def start(self):
        with Listener(on_press=self.on_press,
                      # on_release=self.on_release,
                      suppress=self.suppress) as listener:
            listener.join()


if __name__ == "__main__":
    log = Observer(start_key=None,
                   # set to another special character than Enter to avoid introducing a linebreakbx
                   next_key=Key.enter,
                   stop_key=Key.esc,
                   default_category="Comment",
                   sep="\t",
                   suppress=True,
                   bird={"a": "Bashir",
                         "b": "Brain",
                         "e": "Elie",
                         # "f": "Feisty",
                         "g": "Gigi",
                         "h": "Balbo",
                         # "j": "Jonas",
                         "k": "Kafka",
                         "j": "Jolene",
                         "m": "Merlin",
                         "o": "Osiris",
                         "p": "Pomme",
                         # "r": "Braad",
                         "t": "Tom",
                         # "s": "Siobhan",
                         "r": "Aristotle",
                         "u": "Bussel",
                         "s": "Cassandra",
                         "n": "Connelly",
                         "f": "Fry",
                         "y": "Huxley",
                         "l": "Leo",
                         "x": "Inc"
                         },
                   # # bird={
                   # #     'b': "Booster",
                   # #     'c': "Cern",
                   # #     'k': "Chinook",
                   # #     'd': "Dexter",
                   # #     'q': "Dolcinea",
                   # #     'z': "Gizmo",
                   # #     'g': "Godot",
                   # #     'm': "Homer",
                   # #     'h': "Horatio",
                   # #     'j': "Jaylo",
                   # #     # 'l': "Lima",
                   # #     'e': "Lintie",
                   # #     # 'f': "Lisbon",
                   # #     'n': "Penny",
                   # #     'p': "Poe",
                   # #     'r': "Roland",
                   # #     # 'o': "Rome",
                   # #     't': "Stuka",
                   # #     's': "Sojka",
                   # #     # 'w': "Washington",bc
                   # #     # 'u': "Wellington",
                   # #     'x': 'Inc',
                   # # },
                   noise={
                       "k": "Beak noises",
                       "f": "Wing noises",
                        "b": "Bird vocalisations",
                        "c": "Corvid vocalisations",
                        "a": "Other animal noise",
                        "v": "Car",
                        "p": "Plane",
                        "t": "Train",
                        "s": "Siren",
                        "o": "Voices",
                        "n": "Other human noises",
                        "w": "Wind",
                        "r": "Rain"
                   },
                   states=[
                       "Car",
                       "Train",
                       "Plane",
                       "Siren",
                       "Voices",
                       "Wind",
                       "Rain",
                       "Balbo"
                   ],
                   defaults={
                       "bird": "Pls",
                       "noise": "Other noise",
                   },
                   )
    log.start()
